#pragma once

#include <sqlite_orm/sqlite_orm.h>
#include <filesystem>

namespace AuroraBot::DB {

    struct Server {

        std::string ServerID;

        bool SaveRoles;

        //Is bot must answer only approved roles
        bool ApprovedRoles;

        //Role ID for new users
        std::optional<std::string> AutoRoleID;

        //Category ID for custom channels
        std::optional<std::string> CustomCategoryID;

        //Category ID for main voice and voice(without games) channels
        std::optional<std::string> VoiceCategoryID;

        //Category ID for gaming voice channels
        std::optional<std::string> GamingVoiceCategoryID;

    };

    struct ApprovedRole {

        std::string ServerID;

        std::string RoleID;

    };

    struct VoiceChannel {

        std::string ServerID;

        std::string ChannelID;

        bool WithoutGames;

    };

    struct GamingVoiceChannel {

        std::string ServerID;

        std::string ChannelID;

        //Original name
        std::string Name;

        //Game name
        std::optional<std::string> Game;

    };

    struct CopyCommand {

        //User, that started copying
        std::string UserID;

        //Server, on which started copying
        std::string OriginServerID;

        std::optional<std::string> TargetServerID;

        //Is copying started
        bool Started;

    };

    struct CopyCommandChannel {

        //User from ServerCopyCommand
        std::string UserID;

        std::string OriginChannelID;

        std::optional<std::string> TargetChannelID;

    };

    struct CopyCommandRole {

        //User from ServerCopyCommand
        std::string UserID;

        std::string OriginRoleID;

        std::optional<std::string> TargetRoleID;

    };

    struct MemberLastRole {

        std::string ServerID;

        //Member
        std::string UserID;

        std::string RoleID;

    };

    struct CustomChannel {

        std::string ServerID;

        std::string ChannelID;

        std::optional<std::string> OwnerUserID;

    };

    inline auto GetDB(const std::filesystem::path &dbPath) {
        return sqlite_orm::make_storage(dbPath,
                sqlite_orm::make_table(
                        "Servers",
                        sqlite_orm::make_column("ServerID", &DB::Server::ServerID, sqlite_orm::primary_key()),
                        sqlite_orm::make_column("SaveRoles", &DB::Server::SaveRoles),
                        sqlite_orm::make_column("ApprovedRoles", &DB::Server::ApprovedRoles),
                        sqlite_orm::make_column("AutoRoleID", &DB::Server::AutoRoleID),
                        sqlite_orm::make_column("CustomCategoryID", &DB::Server::CustomCategoryID),
                        sqlite_orm::make_column("VoiceCategoryID", &DB::Server::VoiceCategoryID),
                        sqlite_orm::make_column("GamingVoiceCategoryID", &DB::Server::GamingVoiceCategoryID)
                    ),
                sqlite_orm::make_table(
                        "ApprovedRoles",
                        sqlite_orm::make_column("ServerID", &DB::ApprovedRole::ServerID),
                        sqlite_orm::make_column("RoleID", &DB::ApprovedRole::RoleID),
                        sqlite_orm::foreign_key(&DB::ApprovedRole::RoleID).references(&DB::Server::ServerID).on_update.cascade().on_delete.cascade()
                    ),
                sqlite_orm::make_table(
                        "VoiceChannels",
                        sqlite_orm::make_column("ServerID", &DB::VoiceChannel::ServerID),
                        sqlite_orm::make_column("ChannelID", &DB::VoiceChannel::ChannelID),
                        sqlite_orm::make_column("WithoutGames", &DB::VoiceChannel::WithoutGames),
                        sqlite_orm::foreign_key(&DB::VoiceChannel::ServerID).references(&DB::Server::ServerID).on_update.cascade().on_delete.cascade()
                    ),
                sqlite_orm::make_table(
                        "GamingVoiceChannels",
                        sqlite_orm::make_column("ServerID", &DB::GamingVoiceChannel::ServerID),
                        sqlite_orm::make_column("ChannelID", &DB::GamingVoiceChannel::ChannelID),
                        sqlite_orm::make_column("Name", &DB::GamingVoiceChannel::Name),
                        sqlite_orm::make_column("Game", &DB::GamingVoiceChannel::Game),
                        sqlite_orm::foreign_key(&DB::GamingVoiceChannel::ServerID).references(&DB::Server::ServerID).on_update.cascade().on_delete.cascade()
                    ),
                sqlite_orm::make_table(
                        "CopyCommands",
                        sqlite_orm::make_column("UserID", &DB::CopyCommand::UserID, sqlite_orm::primary_key()),
                        sqlite_orm::make_column("OriginServerID", &DB::CopyCommand::OriginServerID),
                        sqlite_orm::make_column("TargetServerID", &DB::CopyCommand::TargetServerID),
                        sqlite_orm::make_column("Started", &DB::CopyCommand::Started),
                        sqlite_orm::foreign_key(&DB::CopyCommand::OriginServerID).references(&DB::Server::ServerID).on_update.cascade().on_delete.cascade(),
                        sqlite_orm::foreign_key(&DB::CopyCommand::TargetServerID).references(&DB::Server::ServerID).on_update.cascade().on_delete.cascade()
                    ),
                sqlite_orm::make_table(
                        "CopyCommandsChannels",
                        sqlite_orm::make_column("UserID", &DB::CopyCommandChannel::UserID),
                        sqlite_orm::make_column("OriginChannelID", &DB::CopyCommandChannel::OriginChannelID),
                        sqlite_orm::make_column("TargetChannelID", &DB::CopyCommandChannel::TargetChannelID),
                        sqlite_orm::foreign_key(&DB::CopyCommandChannel::UserID).references(&DB::CopyCommand::UserID).on_update.cascade().on_delete.cascade()
                    ),
                sqlite_orm::make_table(
                        "CopyCommandsRoles",
                        sqlite_orm::make_column("UserID", &DB::CopyCommandRole::UserID),
                        sqlite_orm::make_column("OriginRoleID", &DB::CopyCommandRole::OriginRoleID),
                        sqlite_orm::make_column("TargetRoleID", &DB::CopyCommandRole::TargetRoleID),
                        sqlite_orm::foreign_key(&DB::CopyCommandRole::UserID).references(&DB::CopyCommand::UserID).on_update.cascade().on_delete.cascade()
                    ),
                sqlite_orm::make_table(
                        "MembersLastRoles",
                        sqlite_orm::make_column("ServerID", &DB::MemberLastRole::ServerID),
                        sqlite_orm::make_column("UserID", &DB::MemberLastRole::UserID),
                        sqlite_orm::make_column("RoleID", &DB::MemberLastRole::RoleID),
                        sqlite_orm::foreign_key(&DB::MemberLastRole::ServerID).references(&DB::Server::ServerID).on_update.cascade().on_delete.cascade()
                    ),
                sqlite_orm::make_table(
                        "CustomChannels",
                        sqlite_orm::make_column("ServerID", &DB::CustomChannel::ServerID),
                        sqlite_orm::make_column("ChannelID", &DB::CustomChannel::ChannelID),
                        sqlite_orm::make_column("OwnerUserID", &DB::CustomChannel::OwnerUserID),
                        sqlite_orm::foreign_key(&DB::CustomChannel::OwnerUserID).references(&DB::Server::ServerID).on_update.cascade().on_delete.cascade()
                    )
            );
    }

    using DB = decltype(GetDB(""));

}